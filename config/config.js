module.exports = {
    PORT: process.env.PORT,

    // mongoose
    MONGODB_URL: process.env.MONGODB_URL,
    MONGODB_POOLSIZE: process.env.MONGODB_POOLSIZE,

    // redis
    REDIS_HOST: process.env.REDIS_HOST,
    REDIS_PORT: process.env.REDIS_PORT,

    JWT_SECRET: process.env.JWT_SECRET,
    JWT_VALIDITY: process.env.JWT_VALIDITY,
    LOGIN_HARD_LIMIT_ADMIN: process.env.LOGIN_HARD_LIMIT_ADMIN
}