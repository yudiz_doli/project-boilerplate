require('dotenv').config()
const loadEnv = require('dotenv').config({
    path: `./config/envs/${process.env.NODE_ENV || 'development'}.env`
})

const configs = require('./config')
const flags = require('./flags')

if (loadEnv.error) {
    throw loadEnv.error
}

module.exports = {
    ...configs,
    ...flags
}