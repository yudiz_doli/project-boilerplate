const crypto = require('crypto')

const removenull = (obj) => {
  for (var propName in obj) {
    if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
      delete obj[propName]
    }
  }
}

const pick = (object, keys) => {
  return keys.reduce((obj, key) => {
    if (object && object.hasOwnProperty(key)) {
      obj[key] = object[key]
    }
    return obj
  }, {})
}

const validatePassword = (pass) => {
  const regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/
  return regex.test(pass)
}

const validateEmail = (email) => {
  const sRegexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  return sRegexEmail.test(email)
}

const validateMobile = (mobile) => {
  return (/^\d{10}$/).test(mobile)
}

const createHash = (value) => {
  return crypto.createHash('sha512').update(value).digest('hex')
}

const generateCode = (len) => {
  let char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', val = ''

  for (var i = len; i > 0; i--) {
    val += char[Math.floor(Math.random() * char.length)]
  }

  if (val.length === len) {
    return val
  } else {
    generateCode(len)
  }
}

const getTimeDifference = (date) => {
  const now = new Date()
  const diff = now - date
  const mins = Math.ceil(diff / 60000)
  return mins
}

module.exports = {
  removenull,
  pick,
  validateEmail,
  validateMobile,
  validatePassword,
  generateCode,
  createHash,
  getTimeDifference,
}
