const jwt = require('jsonwebtoken')
const config = require('../config')
const AdminModel = require('../modules/admin/admin.model')
const { sendRes } = require('./api-response')

const middlewares = {}

middlewares.isAdminAuthenticated = async (req, res, next) => {
    const sToken = req.headers['authorization']
    if (!sToken) return sendRes(req, res, 'Unauthorized', 'ErrUnauthorized')

    let authData
    try {
        authData = jwt.verify(sToken, config.JWT_SECRET)
        if (!authData) return sendRes(req, res, 'Unauthorized', 'ErrUnauthorized')

        const admin = await AdminModel.findById(authData._id, { aJwtTokens: 1, eStatus: 1 }).lean()
        if (!admin) return sendRes(req, res, 'NotFound', 'NotFound', {}, { '##': 'Admin' })
        if (admin.eStatus === 'B') return sendRes(req, res, 'Unauthorized', 'UserBlocked')
        if (admin.eStatus === 'D') return sendRes(req, res, 'NotFound', 'NotFound', {}, { '##': 'User' })
        const userToken = admin.aJwtTokens
        if (userToken.findIndex(e => e.sToken === sToken) < 0) return sendRes(req, res, 'BadRequest', 'NeedToLoginFirst')

        req.adminId = authData._id
        req.sToken = sToken
        next()
    }
    catch (error) {
        if (error.message === 'jwt expired') {
            return sendRes(req, res, 'Unauthorized', 'NeedToLoginFirst')
        }
        else {
            return sendRes(req, res, 'InternalServerError', 'Error')
        }
    }
}

module.exports = middlewares