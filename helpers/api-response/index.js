const generalEnglish = require('./lang/english/general')
const wordsEnglish = require('./lang/english/words')

const messages = {
    English: {
        ...generalEnglish,
        ...wordsEnglish
    }
}

const status = {
    OK: 200,
    Create: 201,
    Deleted: 204,
    BadRequest: 400,
    Unauthorized: 401,
    NotFound: 404,
    Forbidden: 403,
    NotAcceptable: 406,
    ExpectationFailed: 417,
    Locked: 423,
    InternalServerError: 500,
    UnprocessableEntity: 422,
    ResourceExist: 409,
    TooManyRequest: 429,
    Gone: 410
}

const jsonStatus = {
    OK: 200,
    Create: 201,
    Deleted: 204,
    BadRequest: 400,
    Unauthorized: 401,
    NotFound: 404,
    Forbidden: 403,
    NotAcceptable: 406,
    ExpectationFailed: 417,
    Locked: 423,
    InternalServerError: 500,
    UnprocessableEntity: 422,
    ResourceExist: 409,
    TooManyRequest: 429,
    Gone: 410
}


const sendRes = (req, res, statusEnum, messageEnum, data, wordsEnum, header = undefined) => {
    let getLocaleMessage = messages[req.language][messageEnum]
    // replace words
    if (wordsEnum?.['##']) {
        getLocaleMessage = getLocaleMessage.replace('##', messages[req.language][wordsEnum['##']])
    }

    if (wordsEnum?.['###']) {
        getLocaleMessage = getLocaleMessage.replace('###', messages[req.language][wordsEnum['###']])
    }

    return res.status(status[statusEnum]).set('Authorization', header).json({
        status: jsonStatus[statusEnum],
        messages: getLocaleMessage,
        data
    })
}

module.exports = {
    messages,
    status,
    jsonStatus,
    sendRes
}
