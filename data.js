const enums = {
    supportedLanguages: ['English', 'Hindi'],

    //admin
    adminType: ['SUPER', 'SUB'],
    adminStatus: ['Y', 'B', 'D'],
}

module.exports = enums