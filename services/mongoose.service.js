const mongoose = require('mongoose')
const config = require('../config')

const DBConnection = connection(config.MONGODB_URL, parseInt(config.MONGODB_POOLSIZE))

function connection(DB_URL, maxPoolSize = 10) {
    try {
        let dbConfig = { useNewUrlParser: true, useUnifiedTopology: true, readPreference: 'secondaryPreferred' }
        if (!['dev', 'staging'].includes(process.env.NODE_ENV)) {
            dbConfig = { useNewUrlParser: true, useUnifiedTopology: true, maxPoolSize, readPreference: 'secondaryPreferred' }
        }
        const conn = mongoose.createConnection(DB_URL, dbConfig)
        conn.on('connected', () => {
            // conn.syncIndexes({ background: true })
            console.log(`Connected to ${DB_URL} database...`)
        })
        return conn
    } catch (error) {
        return console.log('Error while connecting DB', error.message)
    }
}

module.exports = {
    DBConnection,
}
