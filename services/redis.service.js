const Redis = require('ioredis')
const config = require('../config/config')
const { handleCatchError } = require('../helpers/utils')

const redisClient = new Redis({
    host: config.REDIS_HOST,
    port: config.REDIS_PORT
})

redisClient.on('error', function (error) {
    console.log('Error in Redis', error)
    handleCatchError(error)
    process.exit(1)
})

redisClient.on('connect', function () {
    console.log('redis connected')
})

module.exports = {
    queuePush: function (queueName, data) {
        return redisClient.rpush(queueName, JSON.stringify(data))
    },
    queuePop: function (queueName) {
        return redisClient.lpop(queueName)
    },
    bulkQueuePop: function (queueName, limit) {
        return redisClient.lpop(queueName, limit)
    },
    queueLen: function (queueName) {
        return redisClient.llen(queueName)
    },
    redisClient,
}