const express = require('express')
const helmet = require('helmet')
const hpp = require('hpp')
const cors = require('cors')
const compression = require('compression')

const config = require('./config')

require('./services/mongoose.service')
require('./services/redis.service')

const app = express()

app.use((req, res, next) => {
    switch (req.header('Language')) {
        case 'en-us':
            req.language = 'English'
            break

        default:
            req.language = 'English'
    }
    next()
})

app.use(hpp())
app.use(cors())
app.use(helmet())
app.disable('x-powered-by')
app.use(express.json())

if (config.COMPRESSION_ENABLED) {
    app.use(compression({
        filter: function (req, res) {
            if (req.headers['x-no-compression']) { // don't compress responses with this request header
                return false
            }
            return compression.filter(req, res)
        }
    }))
}

require('./modules/routes')(app)

app.listen(config.PORT, () => {
    console.log('Hello port👋 ', config.PORT)
})

module.exports =app