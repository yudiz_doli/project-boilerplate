const mongoose = require('mongoose')
const Schema = mongoose.Schema
const { DBConnection } = require('../../services/mongoose.service')

const AdminLogs = new Schema({
    sEmail: { type: String, trim: true, required: true, unique: true },
    bIsVerified: { type: Boolean, default: false },
    sCode: { type: String, trim: true }
}, {
    timestamps: {
        createdAt: 'dCreatedAt',
        updatedAt: 'dUpdatedAt'
    }
})

AdminLogs.index({ sEmail: 1 })

module.exports = DBConnection.model('adminlogs', AdminLogs)