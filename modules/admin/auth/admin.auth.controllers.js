const controllers = {}
const { sendRes } = require('../../../helpers/api-response')
const { adminLogin, updateAdminProfile, adminChangePassword, adminLogout, adminForgotPassword, adminResetPassword,getAdminProfile} = require('./admin.auth.services')

controllers.adminLogin = async (req, res) => {
    try {
        const response = await adminLogin(req.body)
        if(response.title === 'invalidEmailOrPassword') return sendRes(req,res,'Unauthorized', 'anyOneFieldInvalid', {}, { '##': 'Email', '###': 'Password' })
        if(response.title === 'invalidOldPassword') return sendRes(req,res,'Unauthorized', 'anyOneFieldInvalid', {}, { '##': 'Email', '###': 'Password' })
        return sendRes(req,res,'OK', 'LoginSuccessfully', {}, {}, response.data.header)
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

controllers.getAdminProfile = async (req, res) => {
    try {
        const response = await getAdminProfile(req.adminId)
        return sendRes(req, res,'OK', 'FetchedSuccessfully', response.data , { '##': 'Profile' })
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

controllers.updateAdminProfile = async (req, res) => {
    try {
        const response = await updateAdminProfile(req.adminId, req.body)
        if(response.title === 'adminNotFound') return sendRes(req,res,'NotFound', 'NotFound', {}, { '##': 'Admin' })
        return sendRes(req, res,'OK', 'UpdatedSuccessfully', {}, { '##': 'Admin' })
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

controllers.adminChangePassword = async (req, res) => {
    try {
        const response = await adminChangePassword(req.adminId, req.body)
        if(response.title === 'adminNotFound') return sendRes(req,res,'NotFound', 'NotFound', {}, { '##': 'Admin' })
        if(response.title === 'oldPasswordInvalid')return sendRes(req,res, 'Unauthorized', 'IncorrectPassword', {}, { '##': 'OldPassword' })
        return sendRes (req,res,'OK', 'UpdatedSuccessfully', {}, { '##': 'Password' })
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

controllers.adminLogout = async (req, res) => {
    try {
        const response = await adminLogout(req.adminId, req.sToken)
        if(response.title === 'adminNotFound') return sendRes(req,res,'NotFound', 'NotFound', {}, { '##': 'Admin' })
        return sendRes(req, res, 'OK', 'LoggedOutSuccessfully')
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

controllers.adminForgotPassword = async (req, res) => {
    try {
        const response = await adminForgotPassword(req.body)
        if(response.title === 'adminNotFound') return sendRes(req,res,'NotFound', 'NotFound', {}, { '##': 'Admin' })
        if(response.title === 'otpAfterSomeTime') return sendRes(req,res,'BadRequest', 'TryAfterSomeTime')
        return sendRes(req,res,'OK', 'OTPSentSuccess')
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

controllers.adminResetPassword = async (req, res) => {
    try {
        const response = await adminResetPassword(req.body)
        if(response.title === 'invalidRequest') return sendRes(req,res,'BadRequest', 'Invalid', {}, { '##': 'Request' })
        if(response.title === 'OTPExpired') return sendRes(req,res,'Gone', 'OTPExpired')
        if(response.title === 'invalidOtp') return sendRes(req,res,'Unauthorized', 'Invalid', {}, { '##': 'OTP' })
        if(response.title === 'adminNotFound') return sendRes(req,res,'NotFound', 'NotFound', {}, { '##': 'Admin' })
        return sendRes(req, res,'OK', 'PasswordResetSuccess')
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'Error')
    }
}

module.exports = controllers