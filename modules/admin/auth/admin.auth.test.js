const request = require('supertest')
const { describe, it, before } = require('mocha')
const chai = require('chai')
const expect = chai.expect
const server = require('../../../index')
const { messages, status } = require('../../../helpers/api-response/index')
let adminToken = ''

/*
  Make sure to pass corret email and password in success login test case
*/
describe('Create admin auth routes', () => {
  before(async () => { })

  describe('Admin Login Apis', () => {
    let messageToFormat = messages['English']['anyOneFieldInvalid']
    let generalEnum = { '##': 'Email', '###': 'Password' }
    messageToFormat = messageToFormat.replace('##', messages['English'][generalEnum['##']])
    messageToFormat = messageToFormat.replace('###', messages['English'][generalEnum['###']])

    it('Admin should not be Login : blank email and password', (done) => {
      const adminLogin = { sEmail: '', sPassword: '' }
      const wordEnum = { '##': 'Email', '###': 'Password' }
      let fieldsRequired = messages['English']['TwoFieldsRequired']
      fieldsRequired = fieldsRequired.replace('##', messages['English'][wordEnum['##']])
      fieldsRequired = fieldsRequired.replace('###', messages['English'][wordEnum['###']])
      // check validatePassword function for more details
      request(server)
        .post('/api/admin/auth/login')
        .send(adminLogin)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(fieldsRequired)
          done()
        })
    })

    it('Admin should be Login : Correct email and password', (done) => {
      const adminLogin = { sEmail: 'b@v.com', sPassword: '123!@QWerB' }
      const loginSuccess = messages['English']['LoginSuccessfully']
      request(server)
        .post('/api/admin/auth/login')
        .send(adminLogin)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          adminToken = res.header.authorization
          expect(res.body.messages).to.equal(loginSuccess)
          done()
        })
    })

    it('Admin should not be Login : invalid email format', (done) => {
      const adminLogin = { sEmail: 'b1.com', sPassword: '123!@QWer' }
      request(server)
        .post('/api/admin/auth/login')
        .send(adminLogin)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(messageToFormat)
          done()
        })
    })

    it('Admin should not be Login : invalid password format', (done) => {
      const adminLogin = { sEmail: 'b1@d.com', sPassword: '123QWer' }
      request(server)
        .post('/api/admin/auth/login')
        .send(adminLogin)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(messageToFormat)
          done()
        })
    })

    it('Admin should not Login : Correct email and wrong password', (done) => {
      const adminLogin = { sEmail: 'b@v.com', sPassword: '123!@QWe' }
      request(server)
        .post('/api/admin/auth/login')
        .send(adminLogin)
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(messageToFormat)
          done()
        })
    })

    it('Admin should not Login : wrong email and correct password', (done) => {
      const adminLogin = { sEmail: 'b@v.com', sPassword: '123!@QWe' }
      request(server)
        .post('/api/admin/auth/login')
        .send(adminLogin)
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(messageToFormat)
          done()
        })
    })
  })

  describe('Admin Update Api', () => {
    before(async () => { })

    it('Admin update username', (done) => {
      const updateObj = { sUsername: 'bhavin', '##': 'Admin' }
      let updateUserMsg = messages['English']['UpdatedSuccessfully']
      updateUserMsg = updateUserMsg.replace('##', messages['English'][updateObj['##']])
      request(server)
        .put('/api/admin/auth/update')
        .set('authorization', adminToken)
        .send(updateObj)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(updateUserMsg)
          done()
        })
    })

    it('Admin only update username not email: extra field which is not editable', (done) => {
      const userInfo = { sUsername: 'bv', sEmail: 'a@b.com', '##': 'Admin' }
      let updateUserMsg = messages['English']['UpdatedSuccessfully']
      updateUserMsg = updateUserMsg.replace('##', messages['English'][userInfo['##']])
      request(server)
        .put('/api/admin/auth/update')
        .set('authorization', adminToken)
        .send(userInfo)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(updateUserMsg)
          done()
        })
    })

    it('Admin not update username: blank username', (done) => {
      const userInfo = { sUsername: '', '##': 'Username' }
      let unRequired = messages['English']['Required']
      unRequired = unRequired.replace('##', messages['English'][userInfo['##']])
      request(server)
        .put('/api/admin/auth/update')
        .set('authorization', adminToken)
        .send(userInfo)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(unRequired)
          done()
        })
    })
  })

  describe('Admin change password Api', () => {
    before(async () => { })

    it('Admin change password fail : not logged in', (done) => {
      const changePass = { 'sOldPassword': '123!@QW', 'sNewPassword': '123!@QWerB' }
      const unauthorized = messages['English']['ErrUnauthorized']
      request(server)
        .put('/api/admin/auth/change-password')
        .send(changePass)
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(unauthorized)
          done()
        })
    })

    it('Admin change password fail : invalid old password', (done) => {
      const changePass = { 'sOldPassword': '123!@QW', 'sNewPassword': '123!@QWerB' }
      let wrongOldPass = messages['English']['IncorrectPassword']
      const wordsEnum = { '##': 'OldPassword' }
      wrongOldPass = wrongOldPass.replace('##', messages['English'][wordsEnum['##']])
      request(server)
        .put('/api/admin/auth/change-password')
        .set('authorization', adminToken)
        .send(changePass)
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(wrongOldPass)
          done()
        })
    })

    it('Admin change password : no data is passed in body', (done) => {
      let fieldsRequired = messages['English']['TwoFieldsRequired']
      const passwordChangeEnum = { '##': 'NewPassword', '###': 'OldPassword' }
      fieldsRequired = fieldsRequired.replace('##', messages['English'][passwordChangeEnum['##']])
      fieldsRequired = fieldsRequired.replace('###', messages['English'][passwordChangeEnum['###']])
      request(server)
        .put('/api/admin/auth/change-password')
        .set('authorization', adminToken)
        .send({})
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(fieldsRequired)
          done()
        })
    })

    it('Fail : Admin change password fail : new password does not satisfy password criteria', (done) => {
      const changePass = { 'sNewPassword': '12@', 'sOldPassword': '123!@QW' }
      const wrongFormat = messages['English']['InvalidPasswordFormat']
      request(server)
        .put('/api/admin/auth/change-password')
        .set('authorization', adminToken)
        .send(changePass)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(wrongFormat)
          done()
        })
    })

    it('Admin change password success', (done) => {
      const updateObj = { 'sOldPassword': '123!@QWerB', 'sNewPassword': '123!@QWer' }
      let passChanged = messages['English']['UpdatedSuccessfully']
      const wordEnum = { '##': 'Password' }
      passChanged = passChanged.replace('##', messages['English'][wordEnum['##']])
      request(server)
        .put('/api/admin/auth/change-password')
        .set('authorization', adminToken)
        .send(updateObj)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(passChanged)
          done()
        })
    })

    it('Admin change password success', (done) => {
      const updateObj = { 'sOldPassword': '123!@QWer', 'sNewPassword': '123!@QWerB' }
      let passChanged = messages['English']['UpdatedSuccessfully']
      const wordEnum = { '##': 'Password' }
      passChanged = passChanged.replace('##', messages['English'][wordEnum['##']])
      request(server)
        .put('/api/admin/auth/change-password')
        .set('authorization', adminToken)
        .send(updateObj)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(passChanged)
          done()
        })
    })
  })

  describe('Admin forgot password Api', () => {
    before(async () => { })

    it('Admin forgot password success', (done) => {
      const forgotPass = { 'sEmail': 'b@v.com' }
      const OTPSent = messages['English']['OTPSentSuccess']
      request(server)
        .post('/api/admin/auth/forgot-password')
        .send(forgotPass)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(OTPSent)
          done()
        })
    })

    it('Admin forgot password : email does not exist', (done) => {
      const forgotPasswordObj = { sEmail: 'bhavin.v678@gmail.com', '##': 'Admin' }
      let EmailNotFound = messages['English']['NotFound']
      request(server)
        .post('/api/admin/auth/forgot-password')
        .send(forgotPasswordObj)
        .expect(status.NotFound)
        .end(function (err, res) {
          if (err) return done(err)
          EmailNotFound = EmailNotFound.replace('##', messages['English'][forgotPasswordObj['##']])
          expect(res.body.messages).to.equal(EmailNotFound)
          done()
        })
    })

    it('Admin forgot password fail: valid email is not provided', (done) => {
      const forgotPasswordObj = { sEmail: 'bhgmail.com', '##': 'Email' }
      let invalidEmail = messages['English']['Invalid']
      request(server)
        .post('/api/admin/auth/forgot-password')
        .send(forgotPasswordObj)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          invalidEmail = invalidEmail.replace('##', messages['English'][forgotPasswordObj['##']])
          expect(res.body.messages).to.equal(invalidEmail)
          done()
        })
    })

    it('Admin forgot password fail: email is not provided in request', (done) => {
      const forgotPasswordObj = { '##': 'Email' }
      let emailRequired = messages['English']['Required']
      request(server)
        .post('/api/admin/auth/forgot-password')
        .send(forgotPasswordObj)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          emailRequired = emailRequired.replace('##', messages['English'][forgotPasswordObj['##']])
          expect(res.body.messages).to.equal(emailRequired)
          done()
        })
    })

    it('Admin forgot password fail: second request within 5 minutes ', (done) => {
      const forgotPasswordObj = { sEmail: 'b@v.com', '##': 'Email' }
      const TryAfterSomeTime = messages['English']['TryAfterSomeTime']
      request(server)
        .post('/api/admin/auth/forgot-password')
        .send(forgotPasswordObj)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(TryAfterSomeTime)
          done()
        })
    })
  })

  describe('Admin reset password Api', () => {
    before(async () => { })

    it('Admin reset password: invalid email', (done) => {
      const forgotPass = { sEmail: 'hjv' }
      let invalidEmail = messages['English']['Invalid']
      const wordsEnum = { '##': 'Email' }
      invalidEmail = invalidEmail.replace('##', messages['English'][wordsEnum['##']])
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(forgotPass)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(invalidEmail)
          done()
        })
    })

    it('Admin reset password fail: invalid OTP', (done) => {
      const passReset = { sEmail: 'b@v.com', sCode: '0lJ90X' }
      let invalidOTP = messages['English']['Invalid']
      const wordsEnum = { '##': 'OTP' }
      invalidOTP = invalidOTP.replace('##', messages['English'][wordsEnum['##']])
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(passReset)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(invalidOTP)
          done()
        })
    })

    it('Admin reset password fail: invalid Password format', (done) => {
      const passReset = { sEmail: 'b@v.com', sCode: '0lJ90Xs', sPassword: '89@' }
      const invalidFormat = messages['English']['InvalidPasswordFormat']
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(passReset)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(invalidFormat)
          done()
        })
    })

    it('Admin reset password fail: invalid request - passed used code', (done) => {
      const passReset = { sEmail: 'b@v.com', sCode: 'K9nqH6U', sPassword: '123!@QWer' }
      let invalidRequest = messages['English']['Invalid']
      const wordsEnum = { '##': 'Request' }
      invalidRequest = invalidRequest.replace('##', messages['English'][wordsEnum['##']])
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(passReset)
        .expect(status.BadRequest)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(invalidRequest)
          done()
        })
    })

    it('Admin reset password fail: OTP is expired', (done) => {
      const passReset = { sEmail: 'b@v.com', sCode: 'K9nqH6U', sPassword: '123!@QWer' }
      const OTPExpired = messages['English']['OTPExpired']
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(passReset)
        .expect(status.Gone)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(OTPExpired)
          done()
        })
    })

    it('Admin reset password fail: invalid OTP', (done) => {
      const passReset = { sEmail: 'b@v.com', sCode: 'K9nqH6q', sPassword: '123!@QWert' }
      let invalidOTP = messages['English']['Invalid']
      const wordsEnum = { '##': 'OTP' }
      invalidOTP = invalidOTP.replace('##', messages['English'][wordsEnum['##']])
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(passReset)
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(invalidOTP)
          done()
        })
    })

    it('Admin reset password Success: password reset success', (done) => {
      const passReset = { sEmail: 'b@v.com', sCode: 'pVhOiia', sPassword: '123!@QWerB' }
      const passReseted = messages['English']['PasswordResetSuccess']
      request(server)
        .post('/api/admin/auth/reset-password')
        .send(passReset)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(passReseted)
          done()
        })
    })
  })

  describe('Admin Logout Api', () => {
    before(async () => { })

    it('Admin Logout success', (done) => {
      const logOutMsg = messages['English']['LoggedOutSuccessfully']
      request(server)
        .get('/api/admin/auth/logout')
        .set('authorization', adminToken)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(logOutMsg)
          done()
        })
    })

    it('Admin Logout fail, not a valid token', (done) => {
      const unauthorized = messages['English']['ErrUnauthorized']
      request(server)
        .get('/api/admin/auth/logout')
        .set('authorization', '')
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(unauthorized)
          done()
        })
    })
  })

  describe('Admin profile Api', () => {
    before(async () => { })

    it('Admin should fetch profile successfully', (done) => {
      let profileFetched = messages['English']['FetchedSuccessfully']
      const profileData = {
        '_id': '63298c76b6854721c637e265',
        'sUsername': 'bv',
        'sEmail': 'b@v.com',
        'sMobileNumber': '9574939380'
      }
      const wordsEnum = { '##': 'Profile' }
      profileFetched = profileFetched.replace('##', messages['English'][wordsEnum['##']])
      request(server)
        .get('/api/admin/auth/profile')
        .set('authorization', adminToken)
        .expect(status.OK)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(profileFetched)
          expect(res.body.data.profile).to.deep.equal((profileData))
          done()
        })
    })

    it('Admin profile fail: invalid token', (done) => {
      const unauthorized = messages['English']['ErrUnauthorized']
      request(server)
        .get('/api/admin/auth/profile')
        .set('authorization', '')
        .expect(status.Unauthorized)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.body.messages).to.equal(unauthorized)
          done()
        })
    })
  })
})