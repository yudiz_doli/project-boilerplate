const router = require('express').Router()
const validators = require('./admin.auth.validators')
const controllers = require('./admin.auth.controllers')
const middlewares = require('../../../helpers/middlewares')

router.post('/admin/auth/login', validators.adminLogin, controllers.adminLogin)
router.get('/admin/auth/profile', middlewares.isAdminAuthenticated, controllers.getAdminProfile)
router.put('/admin/auth/update', validators.updateAdminProfile, middlewares.isAdminAuthenticated, controllers.updateAdminProfile)
router.put('/admin/auth/change-password', validators.adminChangePassword, middlewares.isAdminAuthenticated, controllers.adminChangePassword)
router.get('/admin/auth/logout', middlewares.isAdminAuthenticated, controllers.adminLogout)
router.post('/admin/auth/forgot-password', validators.adminForgotPassword, controllers.adminForgotPassword)
router.post('/admin/auth/reset-password', validators.adminResetPassword, controllers.adminResetPassword)

module.exports = router