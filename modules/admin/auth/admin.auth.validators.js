const { sendRes } = require('../../../helpers/api-response')
const { validateEmail, validatePassword, pick, removenull } = require('../../../helpers/utils')

const validators = {}

validators.adminLogin = (req, res, next) => {
    try {
        req.body = pick(req.body, ['sEmail', 'sPassword'])
        removenull(req.body)
        const { sEmail, sPassword } = req.body
        if (!sEmail || !sPassword) return sendRes(req, res, 'BadRequest', 'TwoFieldsRequired', {}, { '##': 'Email', '###': 'Password' })
        if (!validateEmail(sEmail)) return sendRes(req, res, 'BadRequest', 'anyOneFieldInvalid', {}, { '##': 'Email', '###': 'Password' })
        if (!validatePassword(sPassword)) return sendRes(req, res, 'BadRequest', 'anyOneFieldInvalid', {}, { '##': 'Email', '###': 'Password' })
        next()
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'error')
    }
}

validators.updateAdminProfile = (req, res, next) => {
    try {
        req.body = pick(req.body, ['sUsername'])
        const { sUsername } = req.body
        if (!sUsername) return sendRes(req, res, 'BadRequest', 'Required', {}, { '##': 'Username' })
        next()
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'error')
    }
}

validators.adminChangePassword = (req, res, next) => {
    try {
        req.body = pick(req.body, ['sOldPassword', 'sNewPassword'])
        const { sOldPassword, sNewPassword } = req.body
        if (!sOldPassword || !sNewPassword) return sendRes(req, res, 'BadRequest', 'TwoFieldsRequired', {}, { '##': 'NewPassword', '###': 'OldPassword' })
        if (!validatePassword(sNewPassword)) return sendRes(req, res, 'BadRequest', 'InvalidPasswordFormat')
        next()
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'error')
    }
}

validators.adminForgotPassword = (req, res, next) => {
    try {
        req.body = pick(req.body, ['sEmail'])
        const { sEmail } = req.body
        if (!sEmail) return sendRes(req, res, 'BadRequest', 'Required', {}, { '##': 'Email' })
        if (!validateEmail(sEmail)) return sendRes(req, res, 'BadRequest', 'Invalid', {}, { '##': 'Email' })
        next()
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'error')
    }
}

validators.adminResetPassword = (req, res, next) => {
    try {
        req.body = pick(req.body, ['sEmail', 'sCode', 'sPassword'])
        const { sEmail, sCode, sPassword } = req.body
        if (!sEmail) return sendRes(req, res, 'BadRequest', 'Required', {}, { '##': 'Email' })
        if (!validateEmail(sEmail)) return sendRes(req, res, 'BadRequest', 'Invalid', {}, { '##': 'Email' })
        if (!sCode) return sendRes(req, res, 'BadRequest', 'Required', {}, { '##': 'OTP' })
        if (sCode.length !== 7) return sendRes(req, res, 'BadRequest', 'Invalid', {}, { '##': 'OTP' })
        if (!sPassword) return sendRes(req, res, 'BadRequest', 'Required', {}, { '##': 'Password' })
        if (!validatePassword(sPassword)) return sendRes(req, res, 'BadRequest', 'InvalidPasswordFormat')
        next()
    } catch (error) {
        return sendRes(req, res, 'InternalServerError', 'error')
    }
}

module.exports = validators