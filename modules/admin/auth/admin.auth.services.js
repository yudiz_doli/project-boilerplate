const AdminModel = require('../admin.model')
const jwt = require('jsonwebtoken')
const config = require('../../../config')
const { createHash, generateCode, getTimeDifference } = require('../../../helpers/utils')
const AdminLogsModel = require('../admin.logs.model')
const { DBConnection } = require('../../../services/mongoose.service')

const adminLogin = async (reqBody) => {
    try {
        const { sEmail, sPassword } = reqBody
        const sHashedPassword = createHash(sPassword)
        let admin = await AdminModel.findOne({ sEmail }, { sPassword: 1, aJwtTokens: 1 }).lean()
        if (!admin) return {title:'invalidEmailOrPassword'}
        if (admin.sPassword !== sHashedPassword) return {title:'invalidOldPassword'}
        const newToken = {
            sToken: jwt.sign({ _id: admin._id }, config.JWT_SECRET, { expiresIn: config.JWT_VALIDITY })
        }
        if (admin.aJwtTokens.length >= config.LOGIN_HARD_LIMIT_ADMIN) {
            admin.aJwtTokens.shift()
            admin.aJwtTokens.push(newToken)
        } else {
            admin.aJwtTokens.push(newToken)
        }
        await AdminModel.findByIdAndUpdate(admin._id, { aJwtTokens: admin.aJwtTokens, dLoginAt: new Date() }, { runValidators: true })
        return {title:'loginSuccessfully',data:{header:newToken.sToken}}
    } catch (error) {
        return Promise.reject(error)
    }
}

const getAdminProfile = async (adminId) => {
    try {
        const profile = await AdminModel.findById(adminId, { sUsername: 1, sEmail: 1, sMobileNumber: 1 }).lean()
        return {title:'profileFetchSucessfully',data :{profile}}
    } catch (error) {
        return Promise.reject(error)
    }
}

const updateAdminProfile = async (adminId, reqBody) => {
    try {
        const admin = await AdminModel.findByIdAndUpdate(adminId, { ...reqBody }, { runValidators: true }).lean()
        if (!admin) return {title:'adminNotFound'}
        return {title:'adminProfileUpdated'}
    } catch (error) {
        return Promise.reject(error)
    }
}

const adminChangePassword = async (adminId, reqBody) => {
    try {
        const { sOldPassword, sNewPassword } = reqBody
        const admin = await AdminModel.findById(adminId, { _id: 0, sPassword: 1 }).lean()
        if (!admin) return {title:'adminNotFound'}
        const sHashedOldPassword = createHash(sOldPassword)
        if (admin.sPassword !== sHashedOldPassword) return {title:'oldPasswordInvalid'}
        const sHashedNewPassword = createHash(sNewPassword)
        await AdminModel.findByIdAndUpdate(adminId, { sPassword: sHashedNewPassword }, { runValidators: true }).lean()
        return {title:'passwordChangeSuccessfully'}
    } catch (error) {
        return Promise.reject(error)
    }
}

const adminLogout = async (adminId, sToken) => {
    try {
        const admin = await AdminModel.findByIdAndUpdate(adminId, { $pull: { aJwtTokens: { sToken } } }, { runValidators: true }).lean()
        if (!admin) return {title:'adminNotFound'}
        return {title:'loggedOutSuccessfully'}
    } catch (error) {
        return Promise.reject(error)
    }
}

const adminForgotPassword = async (reqBody) => {
    try {
        const { sEmail } = reqBody
        const admin = await AdminModel.findOne({ sEmail }, { _id: 1 }).lean()
        if (!admin) return {title:'adminNotFound'}
        const lastLog = await AdminLogsModel.findOne({ sEmail }).sort({ dCreatedAt: -1 }).lean()
        const minutes = getTimeDifference(lastLog.dCreatedAt)
        if (minutes <= 5) return {title:'otpAfterSomeTime'}
        const sCode = generateCode(7)
        await AdminLogsModel.create({
            sEmail,
            sCode
        })

        // send email  
        return {title:'emailSentSuccessfully'}
    } catch (error) {
        return Promise.reject(error)
    }
}

const adminResetPassword = async (reqBody) => {
    const session = await DBConnection.startSession()
    session.startTransaction()
    try {
        const { sEmail, sCode, sPassword } = reqBody
        const log = await AdminLogsModel.findOne({ sEmail }).sort({ dCreatedAt: -1 }).lean()
        if (!log) return {title:'invalidRequest'}
        if (log.bIsVerified) return {title:'invalidRequest'}
        const minutes = getTimeDifference(log.dCreatedAt)
        if (minutes >= 240) return {title:'OTPExpired'}
        if (log.sCode !== sCode) return {title:'invalidOtp'}
        const sHashedPassword = createHash(sPassword)
        const admin = await AdminModel.findOneAndUpdate({ sEmail }, { sPassword: sHashedPassword }, { runValidators: true }).session(session)
        if (!admin) return {title:'adminNotFound'}
        await AdminLogsModel.findOneAndUpdate({ sEmail, sCode }, { bIsVerified: true }, { runValidators: true }).session(session)
        await session.commitTransaction()
        return {'title':'PasswordResetSuccessfully'}
    } catch (error) {
        await session.abortTransaction()
        return Promise.reject(error)
    } finally {
        await session.endSession()
    }
}

module.exports = {
    adminLogin,
    getAdminProfile,
    updateAdminProfile,
    adminChangePassword,
    adminLogout,
    adminForgotPassword,
    adminResetPassword,
}