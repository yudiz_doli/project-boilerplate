const mongoose = require('mongoose')
const Schema = mongoose.Schema
const data = require('../../data')
const { DBConnection } = require('../../services/mongoose.service')

const Admin = new Schema({
    sUsername: { type: String, trim: true, required: true },
    sEmail: { type: String, trim: true, required: true, unique: true },
    sMobileNumber: { type: String, trim: true, required: true },
    eType: { type: String, enum: data.adminType, default: 'SUPER' },
    sPassword: { type: String, trim: true, required: true },
    eStatus: { type: String, enum: data.adminStatus, default: 'Y' },
    aJwtTokens: [{
        sToken: { type: String },
        sPushToken: { type: String, trim: true },
        dTimeStamp: { type: Date, default: Date.now }
    }],
    dLoginAt: { type: Date },
    dPasswordchangeAt: { type: Date }
}, {
    timestamps: {
        createdAt: 'dCreatedAt',
        updatedAt: 'dUpdatedAt'
    }
})

Admin.index({ sEmail: 1 })

module.exports = DBConnection.model('admins', Admin)