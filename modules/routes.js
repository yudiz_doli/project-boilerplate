module.exports = (app) => {
    app.use('/api', [
        require('./admin/auth/admin.auth.routes')
    ])
    app.get('/health-check', (req, res) => {
        const sDate = new Date().toJSON()
        return res.status(200).jsonp({ status: 200, sDate })
    })
    app.get('*', (req, res) => {
        return res.status(404).send('404')
    })
}